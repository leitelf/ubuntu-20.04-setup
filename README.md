# Ubuntu 20.04 setup

Instructions to set up ubuntu 20.04 with Adwaita gnome-shell & gtk theme, Yaru custom icon theme (without the ugly grey folders icons), hardening privacy and security,  configuring nvidia prime, preventing overheating, and installing extra softwares.

### Installation
During installation, select "**Minimal installation**", and check the two options "**Dowload updates while installing Ubuntu**" and "**Install third-party softwares for graphics and Wi-Fi hardware and additional media formats**"

After the installation update the system:
```sh
$ sudo apt update
$ sudo apt full-upgrade
```

Check for bugs:
```sh
$ sudo systemctl --failed
$ sudo journalctl -p 3 -xb
```

### Back to Adwaita
Install gnome tweaks via ubuntu store or terminal
```sh
$ sudo apt install gnome-tweaks
```
Download the Yaru-ll for the custom Yaru icon theme and place the folder in ``/usr/share/icons``.

On gnome tweaks, in the appearance tab, select **Adwaita-dark** for the applications theme, and **Yaru-ll** icons.

To set Adwaita gnome-shell theme, edit the file ``/usr/share/gnome-shell/modes/ubuntu.json`` from
```
{
    "parentMode": "user",
    "stylesheetName": "Yaru/gnome-shell.css",
    "themeResourceName": "theme/Yaru/gnome-shell-theme.gresource",
    "debugFlags": ["backtrace-crashes-all"],
    "enabledExtensions": ["ubuntu-dock@ubuntu.com", "ubuntu-appindicators@ubuntu.com", "desktop-icons@cs>
}
```
to
```
{
    "parentMode": "user",
//    "stylesheetName": "Yaru/gnome-shell.css",
//    "themeResourceName": "theme/Yaru/gnome-shell-theme.gresource",
    "debugFlags": ["backtrace-crashes-all"],
    "enabledExtensions": ["ubuntu-dock@ubuntu.com", "ubuntu-appindicators@ubuntu.com", "desktop-icons@cs>
}
```

At this point the ubuntu session is already customized with the desired themes.

To update the theme used in gdm3 run
```sh
$ sudo update-alternatives --config gdm3-theme.gresource
```

And select the option **1: /usr/share/gnome-shell/gnome-shell-theme.gresource**.

Reboot needed to changes take effect.

### Hardening privacy and security
- Change bluetooth default status to off: ```sudo systemctl disable bluetooth.service ```;
- Install gufw via ubuntu store or terminal and enable the firewall;
- Change privacy settings on ubuntu;
- Setup cloudflare dns [(Tutorial)](https://developers.cloudflare.com/1.1.1.1/1.1.1.1-for-families/setup-instructions/linux/);
- Change privacy settings on firefox (DoH recommended).

### Nvidia on-demand
It's finally working! With the nvidia drivers installed in a hybrid nvidia-intel laptop, open nvidia x server settings, go to **PRIME Profiles** tab and select **NVIDIA On-Demand**. Reboot the system and have fun. :)
### Preventing overheating
Thermald is already installed in ubuntu by default and it should be enough, but it's possible to install tlp and powertop
``` sh
$ sudo apt install tlp tlp-rdw powertop
$ sudo systemctl enable tlp
```
### Extra softwares
For codecs and ms fonts:
``` sh
$ sudo apt ubuntu-restricted-extras dconf-editor
```
And for developers
```sh
$ sudo apt install build-essentil git
```
